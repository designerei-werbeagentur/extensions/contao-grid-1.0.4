<?php

namespace designerei\ContaoGridBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoGridListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ('ce_semantic_html5_start' === $template->getName()) {

            // add grid classes
            $template->class .= ' ce_grid flex-container';
            
            // add gutter class(es)
            if($template->gridGutter) {
                $template->gridGutter = deserialize($template->gridGutter);
                foreach($template->gridGutter as $value) {
                  $template->class .= ' ' . $value;
                }
            }
        }
    }
}
