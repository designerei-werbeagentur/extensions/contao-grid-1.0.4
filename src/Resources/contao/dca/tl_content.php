<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

// $GLOBALS['TL_DCA']['tl_content']['fields']['gridSystem'] = array
// (
//     'exclude'                 => true,
//     'inputType'               => 'select',
//     'options'                 => array(
//                                   'flexbox'
//                                   // 'grid-layout'
//                                 ),
//     'eval'                    => array('tl_class'=>'w50'),
//     'reference'               => &$GLOBALS['TL_LANG']['tl_content']['gridSystem'],
//     'sql'                     => "varchar(32) default 'flexbox'"
// );

$GLOBALS['TL_DCA']['tl_content']['fields']['gridGutter'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'gutter-xs','gutter-sm','gutter-md','gutter-lg','gutter-xl',
                                  'xs:gutter-xs','xs:gutter-sm','xs:gutter-md','xs:gutter-lg','xs:gutter-xl',
                                  'sm:gutter-xs','sm:gutter-sm','sm:gutter-md','sm:gutter-lg','sm:gutter-xl',
                                  'md:gutter-xs','md:gutter-sm','md:gutter-md','md:gutter-lg','md:gutter-xl',
                                  'lg:gutter-xs','lg:gutter-sm','lg:gutter-md','lg:gutter-lg','lg:gutter-xl',
                                  'xl:gutter-xs','xl:gutter-sm','xl:gutter-md','xl:gutter-lg','xl:gutter-xl'
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

PaletteManipulator::create()
  ->removeField('sh5_type', 'html5_legend')
  ->removeField('sh5_additional', 'html5_legend')
  ->addField('gridSystem', 'html5_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('gridGutter', 'html5_legend', PaletteManipulator::POSITION_APPEND)
  ->applyToPalette('sHtml5Start', 'tl_content')
;
